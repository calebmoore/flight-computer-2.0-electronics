# ESS 472 Flight Computer #

This computer is the next iteration of flight computers used for ESS 472 - Rockets and Instrumentation. It is the 5th revision of the computer, built from the ground up to be modern, powerful, and fast.

### What is this repository for? ###

This repository contains the schematics, datasheets, CadSoft EAGLE files, and related documentation for the PCB and computer as a whole.

The computer's firmware can be found [here](https://bitbucket.org/calebmoore/flight-computer-2.0-electronics).

### System Overview ###

#### Features ####

* 32-bit Cortex M0+ processor
* 48 MHz clock speed
* 2 x 3 axis accelerometers (one +/-16g, one +/-200g for launch and impacts)
* 3 axis gyro data
* 3 axis magnetometer 
* 32 Mbit flash storage for flight data
* GPS 
* Interface for high speed telemetry link
* 4 discrete outputs for igniting motors or blowing blast charges (tolerant to 10 A)
* Software configurable flight profile
* Expansion header for interfacing with student built payloads
    * 3 x 12 bit analog-to-digital converter inputs
    * 1 SERCOM module, capable of USART, SPI, or I2C communication
    * pins for commanding the firing of pyro charges externally
    * 3.3V analog and digital supply pins

A quick look at the hardware contained in the computer:

* **Atmel SAM D21J18A** - 32 bit Cortex M0+ microcontroller
* **MPU-9150** - 9-DOF IMU
* **ADXL375** - +/-200g 3-axis accelerometer
* **MSI5611-01BA03** - high resolution altimeter & temperature sensor
* **EM-506** - 24 channel GPS unit
* **AT25DF321A** - 32 Mbit SPI flash

### Contributors ###

* [Caleb Moore](mailto:calebm12@uw.edu)